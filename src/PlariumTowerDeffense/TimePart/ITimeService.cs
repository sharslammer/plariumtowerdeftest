﻿using PlariumTowerDeffense.Cannons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.TimePart
{
	public interface ITimeService
	{
		void Start();

		void Stop();
	}
}
