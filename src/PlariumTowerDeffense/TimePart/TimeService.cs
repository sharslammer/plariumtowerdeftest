﻿using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.MoneyPart;
using PlariumTowerDeffense.Monsters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace PlariumTowerDeffense.TimePart
{
	public class TimeService : ITimeService
	{
		private readonly ICannonControlMediator _cannonMediator;
		private readonly IMonsterControlMediator _monsterMediator;
		private readonly IMoneyControl _moneyControl;
		private Timer _timer;
		private int _tickCount=0;

		public TimeService(ICannonControlMediator cannonMediator, IMonsterControlMediator monsterMediator, IMoneyControl moneyControl)
		{
			_cannonMediator = cannonMediator;
			_monsterMediator = monsterMediator;
			_moneyControl = moneyControl;
		}

		public void Start()
		{
			if (_cannonMediator == null || _monsterMediator == null)
				throw new ArgumentNullException("mediators not implemented");

			Timer timer = new Timer(200);
			_timer = timer;

			_timer.Elapsed += _timer_Elapsed;
			_timer.Start();
		}

		public void Stop()
		{
			_timer.Stop(); 
			_timer.Elapsed -= _timer_Elapsed;

			_timer = null;
		}

		private void _timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			try
			{
				_monsterMediator.Tick();
				_cannonMediator.Tick();
			}
			catch(Exception ex)
			{
			}
			_tickCount++;
			if (_tickCount >= 5)
			{
				_tickCount = 0;
				//raise money
				_moneyControl.EverySecondBonus();
			}
		}
	}
}
