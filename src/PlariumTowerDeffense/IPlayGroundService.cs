﻿using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.PlayGroundService;
using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense
{
	/// <summary>
	/// Control playground/ игровым полем управляет.
	/// </summary>
	public interface IPlayGroundService
	{
		Trail Trail { get; }

		int Height { get; }
		int Width { get; }

		event EventHandler<MoveMonsterEventArgs> MonsterMoved;

		void RegisterCannon(Cell cell, ICannon cannon);

		void MoveTo(Cell outcell, Cell destinationCell, IMonster monster);

		MonsterCell SearchMonsters(ICannon cannon); //пока так.
	}
}
