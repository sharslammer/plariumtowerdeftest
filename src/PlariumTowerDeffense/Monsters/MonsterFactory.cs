﻿using PlariumTowerDeffense.Api.Configuration;
using PlariumTowerDeffense.Monsters.Configuration;
using PlariumTowerDeffense.Monsters.MonsterCreators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters
{
    public class MonsterFactory : IMonsterFactory
    {
		private readonly LevelMonsterConfig _levelMonsterConfig;

		//оставлю это тут переносить на ответственность какбы IoC не хочу.
		private Dictionary<string, IMonsterCreator> factoryMap = new Dictionary<string, IMonsterCreator>
        {
            { MonsterKeys.AttackMonster, new AttackCreatureCreator() },
            { MonsterKeys.CrawlMonster, new CrawlCreatureCreator()},
        };
		private IPlayGroundService _playGroundService;

		public MonsterFactory(IConfigProvider<LevelMonsterConfig> levelMonsterConfig, IPlayGroundService playGroundService)
        {
			_levelMonsterConfig = levelMonsterConfig.Config;
			_playGroundService = playGroundService;
		}

        public IMonster CreateMonster(string key)
        {
			IMonster monster = null;

			if (factoryMap.ContainsKey(key))
            {
                var creator = factoryMap[key];
				var monsterConfig = _levelMonsterConfig.Monsters.FirstOrDefault(x => x.Name == key);
				if (monsterConfig!=null)
				{
					monster = creator.Create(monsterConfig, _playGroundService);
				}
            }

            return monster;
        }
    }
}
