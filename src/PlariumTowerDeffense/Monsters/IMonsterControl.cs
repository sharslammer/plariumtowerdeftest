﻿using System;
using PlariumTowerDeffense.Monsters.Configuration;

namespace PlariumTowerDeffense.Monsters
{
	/// <summary>
	/// interface to control each monster.
	/// </summary>
	public interface IMonsterControlMediator
	{
		void RegisterMonster(IMonster monster);

		void Tick();

		Action MonsterEmptied { get; set; }
	}
}
