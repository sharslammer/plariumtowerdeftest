﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters
{
    public interface IMonster
    {
        event EventHandler<OnDeathEventArgs> Deceased;

		/// <summary>
		/// Current level of damage in liveScores.
		/// </summary>
        int DamageLevel { get; set; }

		int Cost { get; }

		/// <summary>
		/// Force action of monster by external command.
		/// </summary>
		void NextAction();

		void UnderBeat(int p);

		void StopOnFinish();
	}
}
