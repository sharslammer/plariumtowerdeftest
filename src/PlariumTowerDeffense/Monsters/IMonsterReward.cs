﻿namespace PlariumTowerDeffense.Monsters
{
	public interface IMonsterReward
	{
		void Reward(IMonster monster);
	}
}