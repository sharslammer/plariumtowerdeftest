﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters.MonsterActions
{
	public abstract class NextActionStrategy
	{
		public abstract void NextMove();
	}
}
