﻿using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters.MonsterActions
{
	public class MovementStrategy : NextActionStrategy
	{
		private readonly IPlayGroundService _playGroundService;
		private readonly IMonster _monster;

		private readonly HashSet<RoutedCell> _traveledWay = new HashSet<RoutedCell>();
		private readonly HashSet<RoutedCell> _ignoredCells = new HashSet<RoutedCell>();

		private RoutedCell _currentCell;

		private int _ticksToMove;
		private int _currentTick = 0;

		
		public MovementStrategy(int speed, IMonster monster, IPlayGroundService playGroundService)
		{
			_ticksToMove = 100 / speed;
			_playGroundService = playGroundService;
			_monster = monster;

			_currentCell = _playGroundService.Trail.StartCell;
			playGroundService.MoveTo(null, _currentCell, _monster);
			monster.Deceased += monster_Deceased;
		}

		//for testing
		public RoutedCell CurrentCell { get { return _currentCell; } }

		public override void NextMove()
		{
			_currentTick++;
			if(_currentTick>=_ticksToMove)
			{
				_currentTick = 0;

				var newCell = ChooseNextCell();
				_playGroundService.MoveTo(_currentCell, newCell, _monster);

				_currentCell = newCell;

				if (_currentCell == _playGroundService.Trail.FinishCell)
					_monster.StopOnFinish();
			}
		}

		private void monster_Deceased(object sender, OnDeathEventArgs e)
		{
			//IMonster monster = sender as IMonster;
			//if (monster != null)
			//{
				_monster.Deceased -= monster_Deceased;
				_playGroundService.MoveTo(_currentCell, null, _monster);
			//}
		}

		private RoutedCell ChooseNextCell()
		{
			int count = _currentCell.AdjacentCells.Count;
			var list = new List<RoutedCell>(count);
			RoutedCell newCell = null;

			if (_currentCell.AdjacentCells.Contains(_playGroundService.Trail.FinishCell))
				return _playGroundService.Trail.FinishCell;

			foreach (var adjacentCell in _currentCell.AdjacentCells)
			{
				if (_traveledWay.Contains(adjacentCell))
					continue;
				if (_ignoredCells.Contains(adjacentCell))
					continue;

				list.Add(adjacentCell);
			}
			if (list.Count == 0)
			{
				foreach (var adjacentCell in _currentCell.AdjacentCells)
				{
					if (_traveledWay.Contains(adjacentCell))
						continue;

					list.Add(adjacentCell);
				}
			}
			//if no moves go turn down.
			if (list.Count == 0)
			{
				var cell = _traveledWay.Last();
				list.Add(cell);
			}

			//super pseudo random
			int index = (int)(DateTime.Now.Ticks % (long)list.Count);
			newCell = list[index];

			_traveledWay.Add(_currentCell);
			_ignoredCells.UnionWith(_currentCell.AdjacentCells);

			return newCell;
		}
	}
}
