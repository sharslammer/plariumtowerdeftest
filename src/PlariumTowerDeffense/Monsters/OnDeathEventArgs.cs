﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlariumTowerDeffense.Monsters
{
	public class OnDeathEventArgs: EventArgs
	{
		public bool IsFinish { get; set; }
	}
}
