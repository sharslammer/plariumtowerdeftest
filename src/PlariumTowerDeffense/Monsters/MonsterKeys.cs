﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters
{
	public class MonsterKeys
	{
		public const string CrawlMonster = "CrawlMonster";
		public const string AttackMonster = "AttackMonster";
	}
}
