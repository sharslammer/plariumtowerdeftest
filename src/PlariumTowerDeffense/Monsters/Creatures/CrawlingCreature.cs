﻿using PlariumTowerDeffense.Monsters.Configuration;
using PlariumTowerDeffense.Monsters.MonsterActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters
{
	internal class CrawlingCreature : IMonster
	{
		private IMonsterConfiguration _configuration;
		private NextActionStrategy _strategy;

		public event EventHandler<OnDeathEventArgs> Deceased;
		private IPlayGroundService _playGroundService;


		public CrawlingCreature(IMonsterConfiguration configuration, IPlayGroundService playGroundService)
		{
			_configuration = configuration;
			_playGroundService = playGroundService;
			_strategy = new MovementStrategy(_configuration.Speed, this, playGroundService);
		}

		public int DamageLevel { get; set; }

		public int Cost { get { return _configuration.Cost; } }

		public void NextAction()
		{
			_strategy.NextMove();
		}

		public void StopOnFinish()
		{
			OnDeceased(true);
		}

		public void UnderBeat(int p)
		{
			DamageLevel += p;
			if (DamageLevel >= _configuration.Health)
			{
				DamageLevel = _configuration.Health;
				OnDeceased();

				_playGroundService.MoveTo(null, null, this);
			}
		}

		private void OnDeceased(bool isWin = false)
		{
			var handler = Deceased;
			if (handler != null)
				handler(this, new OnDeathEventArgs() { IsFinish = isWin });
		}

	}
}