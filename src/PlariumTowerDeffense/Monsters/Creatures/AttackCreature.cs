﻿using PlariumTowerDeffense.Monsters.Configuration;
using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters
{
    public class AttackCreature: IMonster
    {
        private IMonsterConfiguration _configuration;
		private AttackCreatureState _state;
		private IPlayGroundService _playGroundService;
		private RoutedCell _currentPosition;

		//target to atack
		private object _target;

        public AttackCreature(IMonsterConfiguration configuration, IPlayGroundService playGroundService)
        {
            _configuration = configuration;

        }

		public event EventHandler<OnDeathEventArgs> Deceased;

		public int DamageLevel { get; set; }

		public int Cost { get { return _configuration.Cost; } }

		public void NextAction()
		{
			switch (_state)
			{
				case AttackCreatureState.Walk:
					CheckTarget();
					break;
				case AttackCreatureState.Attacking:
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Determines the target at nearest cells.
		/// </summary>
		private void CheckTarget()
		{
			throw new NotImplementedException();
		}


		public void UnderBeat(int p)
		{
			DamageLevel += p;
			if (DamageLevel >= _configuration.Health)
			{
				DamageLevel = _configuration.Health;
				OnDeceased();
			}
		}

		private void OnDeceased()
		{
			var handler = Deceased;
			if (handler != null)
				handler(this, new OnDeathEventArgs());
		}

		public void StopOnFinish()
		{
			throw new NotImplementedException();
		}
	}

	internal enum AttackCreatureState
	{
		Waiting,
		Walk,
		Attacking,
	}
}
