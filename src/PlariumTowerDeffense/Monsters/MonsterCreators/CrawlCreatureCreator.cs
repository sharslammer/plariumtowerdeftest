﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlariumTowerDeffense.Monsters.Configuration;

namespace PlariumTowerDeffense.Monsters.MonsterCreators
{
    internal class CrawlCreatureCreator : IMonsterCreator
    {
		public IMonster Create(IMonsterConfiguration config, IPlayGroundService playGroundService)
        {
			var monster = new CrawlingCreature(config, playGroundService);

			return monster;
        }
    }
}
