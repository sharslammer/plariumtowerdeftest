﻿using PlariumTowerDeffense.MoneyPart;
using System;

namespace PlariumTowerDeffense.Monsters
{
	public class MonsterReward : IMonsterReward
	{
		private IMoneyControl _moneyControl;

		public MonsterReward(IMoneyControl moneyControl)
		{
			_moneyControl = moneyControl;
		}

		public void Reward(IMonster monster)
		{
			_moneyControl.MonsterKill(monster);
		}
	}
}
