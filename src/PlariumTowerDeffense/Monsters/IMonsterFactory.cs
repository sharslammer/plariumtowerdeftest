﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters
{
    public interface IMonsterFactory
    {
        IMonster CreateMonster(string key);
    }
}
