﻿namespace PlariumTowerDeffense.Monsters.Configuration
{
	/// <summary>
	/// Portion of generation similar monsters.
	/// </summary>
	public class MonsterPortion
	{
		public string MonsterKey { get; set; }

		public int Quantity { get; set; }

		/// <summary>Delay of generation in the portion. in 0.1 second</summary>
		public int Delay { get; set; }
	}
}