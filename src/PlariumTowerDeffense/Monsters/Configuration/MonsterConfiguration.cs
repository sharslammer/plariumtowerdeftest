﻿using System;

namespace PlariumTowerDeffense.Monsters.Configuration
{
	public class MonsterConfiguration : IMonsterConfiguration
	{
		public int Cost { get; set; }

		public int Health { get; set; }

		public string Name { get; set; }

		public int Speed { get; set; }
	}
}