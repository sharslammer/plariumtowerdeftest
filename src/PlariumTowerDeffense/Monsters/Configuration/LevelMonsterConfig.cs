﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters.Configuration
{
	/// <summary>Monster's config of level.</summary>
	public class LevelMonsterConfig
	{
		public List<WaveConfig> Waves { get; set; }

		public List<MonsterConfiguration> Monsters { get; set; }
	}
}
