﻿using System.Collections.Generic;

namespace PlariumTowerDeffense.Monsters.Configuration
{
	public class WaveConfig
	{
		/// <summary>Pause before next wave. in seconds.</summary>
		public int Pause { get; set; }

		public List<MonsterPortion> Portions { get; set; }
	}
}