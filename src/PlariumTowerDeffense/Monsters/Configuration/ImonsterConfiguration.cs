﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Monsters.Configuration
{
    public interface IMonsterConfiguration
    {
        string Name { get; }

        int Speed { get; set; }

        int Cost { get; set; }

        int Health { get; set; }
    }
}
