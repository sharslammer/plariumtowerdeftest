﻿using PlariumTowerDeffense.Monsters.Configuration;

namespace PlariumTowerDeffense.Monsters
{
    public interface IMonsterCreator
    {
		IMonster Create(IMonsterConfiguration config, IPlayGroundService playGroundService);
    }
}