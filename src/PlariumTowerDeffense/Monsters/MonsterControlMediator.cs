﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlariumTowerDeffense.Monsters.Configuration;

namespace PlariumTowerDeffense.Monsters
{
	/// <summary>
	/// Informs monsters about new turn.
	/// </summary>
	public class MonsterControlMediator : IMonsterControlMediator
	{
		//TODO: try multicastdelegate.

		private List<IMonster> _monsters = new List<IMonster>();
		private IMonsterReward _monsterReward;

		public MonsterControlMediator(IMonsterReward monsterReward)
		{
			_monsterReward = monsterReward;
		}

		public Action MonsterEmptied { get; set; }

		public void RegisterMonster(IMonster monster)
		{
			_monsters.Add(monster);
			monster.Deceased += monster_Deceased;
		}

		public void Tick()
		{
			foreach (var monster in _monsters)
			{
				if (monster != null)
					monster.NextAction();
			}
		}

		private void monster_Deceased(object sender, OnDeathEventArgs e)
		{
			IMonster monster = sender as IMonster;
			if (monster != null)
			{
				monster.Deceased -= monster_Deceased;
				DeathMonster(monster);
				if (_monsterReward != null)
					_monsterReward.Reward(monster);
			}
			if (_monsters.Count == 0 && MonsterEmptied!=null)
				MonsterEmptied();
		}

		private void DeathMonster(IMonster monster)
		{
			_monsters.Remove(monster);
		}

	}
}
