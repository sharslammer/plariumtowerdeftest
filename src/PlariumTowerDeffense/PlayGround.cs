﻿using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense
{
    public class PlayGround
    {
        private int _x;
        private int _y;

        public PlayGround(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public void SetTrail(int[] matrixLine, Cell finish)
        {
            ITrailPreparer preparer = new HexTrailPreparer(_x, matrixLine, finish);
        }
    }
}
