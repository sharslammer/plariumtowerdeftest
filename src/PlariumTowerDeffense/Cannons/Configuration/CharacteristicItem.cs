﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
	public class CharacteristicItem<T> : CharacteristicItem
	{
		public T Value { get; set; }
	}

	public abstract class CharacteristicItem
	{
		public string Id { get; set; }

		/// <summary></summary>
		public string Title { get; set; }
	}
}
