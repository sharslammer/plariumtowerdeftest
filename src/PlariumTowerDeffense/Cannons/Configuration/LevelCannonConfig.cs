﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons.Configuration
{
	public class LevelCannonConfig
	{
		public List<CannonConfiguration> CannonConfigurations { get; set; }
	}
}
