﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
	public class CannonCharacteristic
	{
		public CharacteristicItem<int> DamageLevel { get; set; }

		public CharacteristicItem<int> ShootSpeed { get; set; }

		public CharacteristicItem<int> HealthPoints { get; set; }

		public CharacteristicItem<ShootType> ShootType {get;set;}

		public CharacteristicItem<int> ShootRadius { get; set; }
	}
}
