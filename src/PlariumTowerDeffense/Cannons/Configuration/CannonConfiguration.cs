﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons.Configuration
{
	public class CannonConfiguration
	{
		public string CannonName { get; set; }

		public CannonCharacteristic Characteristic { get; set; }

		public int Cost { get; set; }
	}
}
