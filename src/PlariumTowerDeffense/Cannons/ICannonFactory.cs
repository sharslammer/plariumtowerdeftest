﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
	public interface ICannonFactory
	{
		ICannon CreateCannon(string key);
	}
}
