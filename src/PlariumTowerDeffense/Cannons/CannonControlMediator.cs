﻿using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
	public class CannonControlMediator : ICannonControlMediator
	{
		List<ICannon> _cannons = new List<ICannon>();
		private IPlayGroundService _pgService;

		public CannonControlMediator(IPlayGroundService pgService)
		{
			_pgService = pgService;
		}

		public void Tick()
		{
			foreach(var cannon in _cannons)
			{
				var cell = _pgService.SearchMonsters(cannon);
				if (cell != null)
				{
					cannon.Shoot(cell);
				}
			}
		}

		public void RegisterCannon(Cell cell, ICannon cannon)
		{
			_cannons.Add(cannon);
			_pgService.RegisterCannon(cell, cannon);
		}
	}
}
