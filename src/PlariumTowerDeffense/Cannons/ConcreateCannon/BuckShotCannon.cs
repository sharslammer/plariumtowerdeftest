﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons.ConcreateCannon
{
	public class BuckShotCannon: ICannon
	{
		private CannonCharacteristic _characteristic;

		public BuckShotCannon(CannonCharacteristic characteristic)
		{
			_characteristic = characteristic;
		}

		public string Name
		{
			get { return "BuckShotCannon"; }
		}

		public CannonCharacteristic Characteristic
		{
			get { return _characteristic; }
		}

		public void Shoot(PlayGroundService.MonsterCell cell)
		{
			foreach (var monster in cell.Monsters)
			{
				monster.UnderBeat(_characteristic.DamageLevel.Value);
			}
		}
	}
}
