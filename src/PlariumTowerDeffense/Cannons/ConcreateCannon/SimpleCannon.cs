﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons.ConcreateCannon
{
	public class SimpleCannon: ICannon
	{
		private CannonCharacteristic _characteristic;

		public SimpleCannon(CannonCharacteristic characteristic)
		{
			_characteristic = characteristic;
		}

		public string Name
		{
			get { return "SimpleCannon"; }
		}

		public CannonCharacteristic Characteristic
		{
			get { return _characteristic; }
		}

		public int Cost
		{
			get;
			set;
		}


		public void Shoot(PlayGroundService.MonsterCell cell)
		{
			var monster = cell.Monsters.FirstOrDefault();
			monster.UnderBeat(_characteristic.DamageLevel.Value);
		}
	}
}
