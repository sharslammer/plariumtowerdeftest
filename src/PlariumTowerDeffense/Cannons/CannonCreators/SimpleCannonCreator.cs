﻿using PlariumTowerDeffense.Cannons.ConcreateCannon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons.CannonCreators
{
	internal class SimpleCannonCreator : ICannonCreator
	{
		public ICannon CreateCannon(CannonCharacteristic characteristic)
		{
			ICannon cannon = new SimpleCannon(characteristic);
			return cannon; 
		}
	}
}
