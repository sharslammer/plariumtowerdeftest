﻿using PlariumTowerDeffense.Cannons.ConcreateCannon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons.CannonCreators
{
	public class BuckShotCannonCreator : ICannonCreator
	{
		public ICannon CreateCannon(CannonCharacteristic characteristic)
		{
			ICannon cannon = new BuckShotCannon(characteristic);
			return cannon;
		}
	}
}
