﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
    public interface ICannon
    {
		string Name { get; }

		CannonCharacteristic Characteristic { get; }

		//int Cost { get; }

		void Shoot(PlayGroundService.MonsterCell cell);
	}
}
