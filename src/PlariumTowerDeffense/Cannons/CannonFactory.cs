﻿using PlariumTowerDeffense.Cannons.CannonCreators;
using PlariumTowerDeffense.Cannons.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
	public class CannonFactory: ICannonFactory
	{
		private Dictionary<string, ICannonCreator> _creators = new Dictionary<string, ICannonCreator>()
		{
			{"SimpleCannon", new SimpleCannonCreator() },
			{"BuckShotCannon", new BuckShotCannonCreator() },
		};

		private Dictionary<string, CannonCharacteristic> _cannonCharacteristics = new Dictionary<string, CannonCharacteristic>();

		public CannonFactory(IEnumerable<CannonConfiguration> configurations)
		{
			foreach(var c in configurations)
			{
				_cannonCharacteristics.Add(c.CannonName, c.Characteristic);
			}
		}

		public ICannon CreateCannon(string key)
		{
			ICannon cannon = null;
			if(_creators.ContainsKey(key) && _cannonCharacteristics.ContainsKey(key))
			{
				cannon = _creators[key].CreateCannon(_cannonCharacteristics[key]);
			}
			return cannon;
		}
	}
}
