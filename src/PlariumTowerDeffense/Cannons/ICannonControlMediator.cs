﻿using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
	public interface ICannonControlMediator
	{
		void RegisterCannon(Cell cell, ICannon cannon);

		void Tick();
	}
}
