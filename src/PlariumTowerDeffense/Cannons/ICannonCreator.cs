﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
	public interface ICannonCreator
	{
		ICannon CreateCannon(CannonCharacteristic characteristic);
	}
}
