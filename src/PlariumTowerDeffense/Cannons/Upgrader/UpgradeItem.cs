﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Cannons
{
	public class UpgradeItem<T>
	{
		/// <summary>Id of cannon characteristic.</summary>
		public string Id { get; set; }

		public T Diff { get; set; }
	}
}
