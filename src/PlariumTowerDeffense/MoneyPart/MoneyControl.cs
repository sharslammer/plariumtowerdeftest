﻿using PlariumTowerDeffense.Api.Configuration;
using PlariumTowerDeffense.ApiConfigs;
using PlariumTowerDeffense.Monsters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.MoneyPart
{
	public class MoneyControl : IMoneyControl
	{
		public event EventHandler<MoneyChangedArgs> MoneyChanged;
		private int _reward;

		public MoneyControl(IConfigProvider<LevelConfig> levelConfig)
		{
			_reward = levelConfig.Config.EverySecondReward;
			CurrentGold = levelConfig.Config.StartGold;
		}

		public bool BuyCannon(int cost)
		{
			bool result = false;
			if (CurrentGold > cost)
			{
				CurrentGold -= cost;
				result = true;
				OnMoneyChanged();
			}
			return result;
		}

		public void EverySecondBonus()
		{
			CurrentGold += _reward;
			OnMoneyChanged();
		}

		public void MonsterKill(IMonster monster)
		{
			var reward = monster.Cost;
			CurrentGold += reward;
			OnMoneyChanged();
		}

		public void RepairCannon()
		{
			throw new NotImplementedException();
		}

		public void UpgradeCannon()
		{
			throw new NotImplementedException();
		}

		public int CurrentGold
		{
			get;
			private set;
		}

		private void OnMoneyChanged()
		{
			var handler = MoneyChanged;
			if (handler != null)
				handler(this, new MoneyChangedArgs(CurrentGold));
		}
	}
}
