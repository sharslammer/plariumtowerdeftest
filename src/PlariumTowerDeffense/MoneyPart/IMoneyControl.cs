﻿using PlariumTowerDeffense.Monsters;
using System;

namespace PlariumTowerDeffense.MoneyPart
{
	public interface IMoneyControl
	{
		event EventHandler<MoneyChangedArgs> MoneyChanged;

		int CurrentGold { get; }

		bool BuyCannon(int cost);

		void EverySecondBonus();

		void MonsterKill(IMonster monster);

		void RepairCannon();

		void UpgradeCannon();
	}
}
