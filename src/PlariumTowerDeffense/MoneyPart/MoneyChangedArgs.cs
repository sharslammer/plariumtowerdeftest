﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlariumTowerDeffense.MoneyPart
{
	public class MoneyChangedArgs: EventArgs
	{
		public MoneyChangedArgs(int newValue)
		{
			NewValue = newValue;
		}

		public int NewValue { get; private set; }
	}
}
