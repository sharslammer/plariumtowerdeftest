﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.ApiConfigs
{
	public class LevelConfig
	{
		public int EverySecondReward { get; set; }

		public int StartGold { get; set; }
	}
}
