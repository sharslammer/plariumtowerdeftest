﻿using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.ApiConfigs
{
    public class PlayGroundConfig
    {
        /// <summary>Number of horizontal cells.</summary>
        public int Width { get; set; }

        /// <summary>Number of vertical cells.</summary>
        public int Height { get; set; }

        public Cell FinishCell { get; set; }

        public int[] MatrixLine { get; set; }
    }
}
