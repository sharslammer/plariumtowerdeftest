﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.LevelService
{
	public interface ILevelMainService
	{
		event EventHandler<NewWaveStartedArgs> NewWaveStarted;

		event EventHandler<MonsterCreatedArgs> NewMonsterCreated;

		Action EndWaves { get; set; }

		void StartMonsterWave();
	}
}
