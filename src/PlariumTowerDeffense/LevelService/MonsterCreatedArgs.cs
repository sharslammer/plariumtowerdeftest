﻿using PlariumTowerDeffense.Monsters;

namespace PlariumTowerDeffense.LevelService
{
	public class MonsterCreatedArgs
	{
		public IMonster Monster {get;set; }
	}
}