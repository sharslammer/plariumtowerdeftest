﻿using PlariumTowerDeffense.Monsters.Configuration;

namespace PlariumTowerDeffense.LevelService
{
	public class NewWaveStartedArgs
	{
		public WaveConfig Config { get; set; }
	}
}