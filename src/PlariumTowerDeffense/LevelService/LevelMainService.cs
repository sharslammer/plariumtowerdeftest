﻿using PlariumTowerDeffense.Monsters.Configuration;
using System;
using System.Threading.Tasks;
using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.Api.Configuration;

namespace PlariumTowerDeffense.LevelService
{
	public class LevelMainService: ILevelMainService
	{
		private LevelMonsterConfig _config;
		private IMonsterFactory _monsterFactory;
		private readonly IMonsterControlMediator _mediator;
		private int _currentWave;

		private bool isWave;
		
		public event EventHandler<NewWaveStartedArgs> NewWaveStarted;
		public event EventHandler<MonsterCreatedArgs> NewMonsterCreated;

		public LevelMainService(IConfigProvider<LevelMonsterConfig> config, IMonsterFactory monsterFactory, IMonsterControlMediator mediator)
		{
			_config = config.Config;
			_monsterFactory = monsterFactory;
			_mediator = mediator;
			_mediator.MonsterEmptied = StartNextWave;
		}

		public Action EndWaves { get; set; }

		public void StartMonsterWave()
		{
			StartMonsterWave(0);
		}

		public void StartMonsterWave(int wave)
		{
			_currentWave = wave;
			Task.Run(() => GenearateWaveTask(_config.Waves[wave]));
		}

		private async Task GenearateWaveTask(WaveConfig waveConfig)
		{
			isWave = true;

			var handler = NewWaveStarted;
			if (handler != null)
				NewWaveStarted(this, new NewWaveStartedArgs() { Config = waveConfig });
			var monsterHandler = NewMonsterCreated;

			//delay on StartMonsterWave wave
			await Task.Delay(waveConfig.Pause * 1000);
			foreach (var portion in waveConfig.Portions)
			{
				for (int k = 0; k < portion.Quantity; k++)
				{
					var monster = _monsterFactory.CreateMonster(portion.MonsterKey);
					_mediator.RegisterMonster(monster);

					if (monsterHandler != null)
						monsterHandler(this, new MonsterCreatedArgs() { Monster = monster });

					if (portion.Delay > 0)
						await Task.Delay(100 * portion.Delay);
				}
			}
			isWave = false;
		}

		private void StartNextWave()
		{
			if (!isWave)
			{
				if (_currentWave + 1 < _config.Waves.Count)
				{
					StartMonsterWave(_currentWave + 1);
				}
				else
				{
					if(EndWaves!=null)
						EndWaves();
				}
			}
		}

	}
}
