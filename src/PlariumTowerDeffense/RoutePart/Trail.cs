﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.RoutePart
{
    /// <summary>
    /// Descriptor of trail.
    /// </summary>
    public class Trail
    {
		private RoutedCell _startCell;
        private RoutedCell _finishCell;
        private List<RoutedCell> _cells = new List<RoutedCell>();

        public Trail(IEnumerable<RoutedCell> cells, RoutedCell finishCell)
        {
            _cells = cells.ToList();

			_startCell = cells.First(c => c.X == 0 && c.Y == 0);
			_finishCell = finishCell;
        }

		public RoutedCell StartCell { get { return _startCell; } }
		public RoutedCell FinishCell { get { return _finishCell; } }

        public List<RoutedCell> TrailCells { get { return _cells; } }
    }
}
