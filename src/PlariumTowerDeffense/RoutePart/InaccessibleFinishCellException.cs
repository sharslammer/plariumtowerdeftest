﻿using System;
using System.Runtime.Serialization;

namespace PlariumTowerDeffense.RoutePart
{
    [Serializable]
    internal class InaccessibleFinishCellException : Exception
    {
        public InaccessibleFinishCellException()
        {
        }

        public InaccessibleFinishCellException(string message) : base(message)
        {
        }

        public InaccessibleFinishCellException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InaccessibleFinishCellException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}