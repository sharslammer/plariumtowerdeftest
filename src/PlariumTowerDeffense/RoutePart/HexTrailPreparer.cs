﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.RoutePart
{

    public class HexTrailPreparer : ITrailPreparer
    {
        private Cell _finishCell;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="x">Ширина игрового поля.</param>
        /// <param name="matrixLine">Матрица смежности.</param>
        public HexTrailPreparer(int x, int[] matrixLine, Cell finishCell)
        {
            _finishCell = finishCell;

        }

        public Trail Prepare(int x, int[] matrixLine)
        {
            RoutedCell startCell = new RoutedCell(0, 0);
            var cells = ReadCells(x, matrixLine);
            ScanNearestCells(cells);
            var finish = cells.FirstOrDefault(c => c.X == _finishCell.X && c.Y == _finishCell.Y);

            if (finish == null)
                throw new InaccessibleFinishCellException();

            Trail trail = new Trail(cells, finish);

            return trail;
        }

        private void ScanNearestCells(List<RoutedCell> cells)
        {
            foreach (var cell in cells)
            {
                if (cell.X % 2 == 0)
                {
                    var items = cells.Where(c =>
                            //current row
                            (c.X == cell.X && Math.Abs((c.Y - cell.Y)) == 1)
                            //aboverow
                            || (c.X == cell.X - 1 && ((cell.Y == c.Y) || (cell.Y - c.Y) == 1))
                            //belowrow
                            || (c.X == cell.X + 1 && ((cell.Y == c.Y) || (cell.Y - c.Y) == 1))
                        );
                    cell.AdjacentCells.AddRange(items);
                }
                else
                {
                    var items = cells.Where(c =>
                            //current row
                            (c.X == cell.X && Math.Abs((c.Y - cell.Y)) == 1)
                            //aboverow
                            || (c.X == cell.X - 1 && ((cell.Y == c.Y) || (cell.Y - c.Y) == -1))
                            //belowrow
                            || (c.X == cell.X + 1 && ((cell.Y == c.Y) || (cell.Y - c.Y) == -1))
                        );
                    cell.AdjacentCells.AddRange(items);
                }
            }
        }

        private List<RoutedCell> ReadCells(int x, int[] matrixLine)
        {
            List<RoutedCell> cells = new List<RoutedCell>(matrixLine.Length);

            for (int i = 0; i < matrixLine.Length; i++)
            {
                if (matrixLine[i] > 0)
                {
                    var cell = new RoutedCell(i / x, i % x);
                    cells.Add(cell);
                }
            }

            return cells;
        }
    }
}
