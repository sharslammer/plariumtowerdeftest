﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.RoutePart
{
    public interface ITrailPreparer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="matrixLine"></param>
        /// <exception cref="InaccessibleFinishCellException" ></exception>
        /// <returns></returns>
        Trail Prepare(int x, int[] matrixLine);
    }
}
