﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.RoutePart
{
    /// <summary>
    /// This cell is oriented to routing.
    /// </summary>
    public class RoutedCell : Cell
    { 
        //смежные ячейки.
        List<RoutedCell> _adjacentCells = new List<RoutedCell>();

        public RoutedCell(int x, int y) : base(x, y) { }

        public RoutedCell(RoutedCell startCell): base(startCell.X, startCell.Y)
        {
        }


        public List<RoutedCell> AdjacentCells
        {
            get { return _adjacentCells; }
        }

    }
}
