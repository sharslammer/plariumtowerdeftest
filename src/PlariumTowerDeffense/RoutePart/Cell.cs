﻿namespace PlariumTowerDeffense.RoutePart
{
    /// <summary>
    /// Descriptor of cell of trail.
    /// Cell is anabled for monster route.
    /// </summary>
    public class Cell
    {
        private int _x;
        private int _y;

        public Cell(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public int X
        {
            get { return _x; }
        }

        public int Y
        {
            get { return _y; }
        }

		//public static bool operator == (Cell obj, Cell second)
		//{
		//	if ((object)obj == null)
		//		return (object)second == null;

		//	if ((object)second == null)
		//		return (object)obj == null;

		//	if (obj.X == second.X && obj.Y == second.Y)
		//		return true;

		//	return false;
		//}

		//public static bool operator !=(Cell obj, Cell second)
		//{
		//	return !(obj == second);
		//}
    }
}