﻿using PlariumTowerDeffense.Api.Configuration;
using PlariumTowerDeffense.ApiConfigs;
using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PlariumTowerDeffense.PlayGroundService
{
	public class PlayGroundService: IPlayGroundService
	{
		Dictionary<ICannon, CannonCell> _cannons = new Dictionary<ICannon, CannonCell>();
		List<ContentCell> _contentCells = new List<ContentCell>();
		private readonly PlayGroundConfig _config;
		private Trail _trail;

		public event EventHandler<MoveMonsterEventArgs> MonsterMoved;

		public PlayGroundService(IConfigProvider<PlayGroundConfig> pgConfig)
		{
			_config = pgConfig.Config;
			SetTrail();

			var localToCell = new MonsterCell(Trail.StartCell);
			_contentCells.Add(localToCell);
		}

		public PlayGroundConfig Config { get { return _config; } }

		public Trail Trail { get { return _trail; } }

		public int Height { get { return _config.Height; } }

		public int Width { get { return _config.Width; } }

		public void RegisterCannon(Cell cell, ICannon cannon)
		{
			CannonCell cCell = new CannonCell(cell, cannon);
			_contentCells.Add(cCell);
			_cannons.Add(cannon, cCell);
			//determine nearest cells at trail. only 1 radius cell.
			cCell.Amplitude = GetNearestCellsOnTrail(cell).ToList();
		}

		public void StartMonster()
		{

		}

		public void MoveTo(Cell outcell, Cell destinationCell, IMonster monster)
		{
			var localFromCell = _contentCells.FirstOrDefault(c => c.Cell == outcell) as MonsterCell;
			var localToCell = _contentCells.FirstOrDefault(c => c.Cell == destinationCell) as  MonsterCell;

			Cell outCell = null;
			if (localFromCell != null)
			{
				localFromCell.MonsterCameOut(monster);
				outCell = localFromCell.Cell;
			}

			if (localToCell == null)
			{
				localToCell = new MonsterCell(destinationCell);
				_contentCells.Add(localToCell);
			}
			localToCell.MonsterEntered(monster);

			OnMonsterMoved(outCell, localToCell.Cell, monster);
		}

		public MonsterCell SearchMonsters(ICannon cannon)
		{
			MonsterCell result = null;
			if (_cannons.ContainsKey(cannon))
			{
				var cell = _cannons[cannon];
				foreach (var nearCell in cell.Amplitude)
				{
					var monsterCell = _contentCells.FirstOrDefault(c => c.Cell == nearCell) as MonsterCell;
					if (monsterCell != null && !monsterCell.IsEmpty)
					{
						result = monsterCell;
						break;
					}
				}
			}

			return result;
		}

		private void SetTrail()
		{
			ITrailPreparer preparer = new HexTrailPreparer(_config.Width, _config.MatrixLine, _config.FinishCell);
			_trail = preparer.Prepare(_config.Width, _config.MatrixLine);
		}

		private void OnMonsterMoved(Cell outcell, Cell destinationCell, IMonster monster)
		{
			var handler = MonsterMoved;
			if (handler != null)
				handler(this, new MoveMonsterEventArgs(outcell, destinationCell, monster));
		}

		private IEnumerable<Cell> GetNearestCellsOnTrail(Cell cell)
		{
			var cells = _trail.TrailCells;
			if (cell.X % 2 == 0)
			{
				return cells.Where(c =>
					//current row
						(c.X == cell.X && Math.Abs((c.Y - cell.Y)) == 1)
							//aboverow
						|| (c.X == cell.X - 1 && ((cell.Y == c.Y) || (cell.Y - c.Y) == 1))
							//belowrow
						|| (c.X == cell.X + 1 && ((cell.Y == c.Y) || (cell.Y - c.Y) == 1))
					);
				//cell.AdjacentCells.AddRange(items);
			}
			else
			{
				return cells.Where(c =>
					//current row
						(c.X == cell.X && Math.Abs((c.Y - cell.Y)) == 1)
							//aboverow
						|| (c.X == cell.X - 1 && ((cell.Y == c.Y) || (cell.Y - c.Y) == -1))
							//belowrow
						|| (c.X == cell.X + 1 && ((cell.Y == c.Y) || (cell.Y - c.Y) == -1))
					);
				//cell.AdjacentCells.AddRange(items);
			}
		}
	}
}
