﻿using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.PlayGroundService
{
	public class MonsterCell : ContentCell
	{
		private List<IMonster> _monsters = new List<IMonster>();

		public MonsterCell(Cell cell) : base(cell) { }

		public bool IsEmpty { get { return _monsters.Count == 0; } }

		public List<IMonster> Monsters { get { return _monsters; } }

		public void MonsterEntered(IMonster monster)
		{
			_monsters.Add(monster);
		}

		public void MonsterCameOut(IMonster monster)
		{
			_monsters.Remove(monster);
		}
	}
}
