﻿using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.PlayGroundService
{
	public class CannonCell: ContentCell
	{
		private ICannon _cannon;

		public CannonCell(Cell cell, ICannon cannon) :
			base(cell)
		{
			_cannon = cannon;
		}

		public List<Cell> Amplitude { get; set; }
		//OnDestroy/
	}
}
