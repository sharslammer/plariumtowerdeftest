﻿using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.PlayGroundService
{
	/// <summary>
	/// Cell with anything on it.
	/// </summary>
	public class ContentCell
	{
		protected readonly Cell _cell;

		public ContentCell(Cell cell)
		{
			_cell = cell;
		}

		public Cell Cell { get { return _cell; } }

	}
}
