﻿using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.RoutePart;
using System;

namespace PlariumTowerDeffense
{
	public class MoveMonsterEventArgs: EventArgs
	{
		public MoveMonsterEventArgs(Cell outcell, Cell destinationCell, IMonster monster)
		{
			OutCell = outcell;
			DestinationCell = destinationCell;
			Monster = monster;
		}

		public Cell OutCell { get; private set; }
		public Cell DestinationCell { get; private set; }
		public IMonster Monster { get; private set; }
	}
}