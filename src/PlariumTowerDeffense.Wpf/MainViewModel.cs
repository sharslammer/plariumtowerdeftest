﻿using PlariumTowerDeffense.ApiConfigs;
using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.Cannons.Configuration;
using PlariumTowerDeffense.LevelService;
using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.Monsters.Configuration;
using PlariumTowerDeffense.RoutePart;
using PlariumTowerDeffense.TimePart;
using PlariumTowerDeffense.Wpf.Common;
using PlariumTowerDeffense.Wpf.ConfigPart;
using PlariumTowerDeffense.Wpf.InitializationConfigs;
using PlariumTowerDeffense.Wpf.MonsterPanel;
using PlariumTowerDeffense.Wpf.PlayerPanel;
using PlariumTowerDeffense.Wpf.PlayGround;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PlariumTowerDeffense.Wpf
{
	public class MainViewModel : ViewModel
	{
		private ITimeService _timeService;

		public MainViewModel()
		{
			IsStartMenuShow = true;

			SimpleResolveConfigs();
		}
		
		public PlayGroundViewModel PlayGroundVm { get; private set; }

		public PlayerPanelViewModel PlayerVm { get; private set; }

		public ConfigViewModel ConfigVm { get; private set; }

		public MonsterPanelViewModel MonsterPanelVm { get; private set; }

		public bool IsEndGameMenuShow { get; private set; }

		public string EndGameTitle { get; private set; }
		
		public bool IsStartMenuShow { get; private set; }

		public bool IsConfigMenu { get; private set; }

		public ICommand CommandStart { get { return new UiCommand(StartExec); } }
		public ICommand CommandRestart { get { return new UiCommand(RestartExec); } }
		public ICommand CommandStop { get { return new UiCommand(StopExec); } }
		public ICommand CommandShowConfig { get { return new UiCommand(ShowConfig); } }

		private void ShowConfig()
		{
			IsStartMenuShow = false;
			OnPropertyChanged(() => IsStartMenuShow);
			IsConfigMenu = true;
			OnPropertyChanged(() => IsConfigMenu);

			if (ConfigVm == null)
			{
				var list = GetConfigProviders().ToList();
				ConfigVm = new ConfigViewModel(list);
				OnPropertyChanged(() => ConfigVm);
			}
		}

		private void StartExec()
		{
			IsStartMenuShow = false;
			OnPropertyChanged(() => IsStartMenuShow);

			var selectionMediator = IocMock.ResolveSelectionMediator();

			//draw playground
			IPlayGroundService pgService = IocMock.ResolvePlayGroundService();
			PlayGroundVm = new PlayGroundViewModel(pgService, selectionMediator, LoseLevel);
			OnPropertyChanged(() => PlayGroundVm);
			//playerpanel 
			var cannonMediator = IocMock.ResolveCannonControlMediator();
			var moneyControl = IocMock.ResolveMoneyControl();
			var cannonFactory = IocMock.ResolveCannonFactory();

			var levelCannonConfig = IocMock.ResolveConfigProvider<LevelCannonConfig>();

			PlayerVm = new PlayerPanelViewModel(selectionMediator, moneyControl, cannonMediator, cannonFactory, levelCannonConfig.Config.CannonConfigurations);
			OnPropertyChanged(() => PlayerVm);

			//start geme timers
			_timeService = IocMock.ResolveTimeService();
			_timeService.Start();

			ILevelMainService service = IocMock.ResolveLevelMainService();
			service.EndWaves = WinLevel;
			service.StartMonsterWave();

			MonsterPanelVm = new MonsterPanelViewModel(service);
			OnPropertyChanged(() => MonsterPanelVm);
		}

		private void WinLevel()
		{
			IsEndGameMenuShow = true;
			EndGameTitle = "YOU WIN!!!" + Environment.NewLine + "WIN!!!";
			OnPropertyChanged(() => IsEndGameMenuShow);
			OnPropertyChanged(() => EndGameTitle);

			_timeService.Stop();
			IocMock.Kill();
		}

		private void LoseLevel()
		{
			IsEndGameMenuShow = true;
			EndGameTitle = "YOU LOSE!!!";
			OnPropertyChanged(() => IsEndGameMenuShow);
			OnPropertyChanged(() => EndGameTitle);

			_timeService.Stop();
			IocMock.Kill();
		}

		private void RestartExec()
		{
			IsEndGameMenuShow = false;
			OnPropertyChanged(() => IsEndGameMenuShow);

			StartExec();
		}

		private void StopExec()
		{
			IsEndGameMenuShow = false;
			OnPropertyChanged(() => IsEndGameMenuShow);
			IsStartMenuShow = true;
			OnPropertyChanged(() => IsStartMenuShow);
			IsConfigMenu = false;
			OnPropertyChanged(() => IsConfigMenu);
		}

		/// <summary>
		/// простенький способ определить наличие конфигов.
		/// если нет - то заколбасим какие-нибудь.
		/// </summary>
		private void SimpleResolveConfigs()
		{
			string path = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "config");
			//check if config path exists
			if(!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);

				LevelConfig levelConfig = new LevelConfig()
				{
					EverySecondReward = 20,
					StartGold = 500,
				};

				InititalizationConfigProvider<LevelConfig> lc = new InititalizationConfigProvider<LevelConfig>(path, levelConfig);
				lc.CreateConfig();

				LevelCannonConfigInit lcc = new LevelCannonConfigInit(path, null);
				lcc.CreateConfig();

				LevelMonsterConfigInit lmc = new LevelMonsterConfigInit(path, null);
				lmc.CreateConfig();

				PlayGroundConfigInit pgc = new PlayGroundConfigInit(path, null);
				pgc.CreateConfig();
			}
		}

		/// <summary>
		/// ну что ж тут мы "знаем" какие есть конфиги, это не совсем хорошо, но так быстрее.
		/// </summary>
		private IEnumerable<IInititalizationConfigProvider> GetConfigProviders()
		{
			string path = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "config");
			InititalizationConfigProvider<LevelConfig> lc = new InititalizationConfigProvider<LevelConfig>(path, null);
			yield return lc;

			LevelCannonConfigInit lcc = new LevelCannonConfigInit(path, null);
			yield return lcc;

			LevelMonsterConfigInit lmc = new LevelMonsterConfigInit(path, null);
			yield return lmc;

			PlayGroundConfigInit pgc = new PlayGroundConfigInit(path, null);
			yield return pgc;
		}
	}
}
