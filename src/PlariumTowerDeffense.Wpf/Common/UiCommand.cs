﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PlariumTowerDeffense.Wpf.Common
{
	public class UiCommand : ICommand
	{
		private readonly Action onInvoke;
		private readonly Func<bool> canExecute;

		/// <summary>
		/// Создает экземпляр <see cref="UiCommand"/>
		/// </summary>
		/// <param name="onInvoke">Действие, выполняемое при пользовательском воздействии</param>
		public UiCommand(Action onInvoke)
		{
			this.onInvoke = onInvoke;
		}

		/// <summary>
		/// Создает экземпляр <see cref="UiCommand"/>
		/// </summary>
		/// <param name="onInvoke">Действие, выполняемое при пользовательском воздействии</param>
		/// <param name="canExecute">Действие, выполняемое для проверки возможности вызова onInvoke</param>
		public UiCommand(Action onInvoke, Func<bool> canExecute)
		{
			this.onInvoke = onInvoke;
			this.canExecute = canExecute;
		}

		/// <summary>
		/// Проверяет возможность вызова onInvoke
		/// </summary>
		/// <param name="parameter">Параметр, передаваемый из UI слоя</param>
		/// <returns>
		/// <b>true</b> - в случае возможности вызова onInvoke
		/// <b>false</b> - в случае невозможности
		/// </returns>
		public bool CanExecute(object parameter)
		{
			if (this.canExecute == null)
				return true;

			var result = this.canExecute.Invoke();
			return result;
		}

		/// <summary>
		/// Вызывает onInvoke
		/// </summary>
		/// <param name="parameter">Параметр, передаваемый из UI слоя</param>
		public void Execute(object parameter)
		{
			if (!this.CanExecute(parameter) || this.onInvoke == null)
				return;

			this.onInvoke.Invoke();
		}

		public void RaiseCanExecuteChanged()
		{
			CommandManager.InvalidateRequerySuggested();
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				if (this.onInvoke == null || this.canExecute == null)
					return;
				CommandManager.RequerySuggested += value;
			}
			remove
			{
				if (this.onInvoke == null || this.canExecute == null)
					return;
				CommandManager.RequerySuggested -= value;
			}
		}		
	}
}
