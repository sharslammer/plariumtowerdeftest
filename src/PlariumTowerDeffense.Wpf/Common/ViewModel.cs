﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace PlariumTowerDeffense.Wpf.Common
{
	public class ViewModel: INotifyPropertyChanged
	{
		private Dispatcher _dispatcher;

		/// <summary>
		/// Выполняет отложенный вызов на UI потоке
		/// </summary>
		/// <param name="action">Выполняемое действие</param>
		/// <param name="priority">Приоритет диспетчера</param>
		protected void InvokeUiThread(Action action, DispatcherPriority priority = DispatcherPriority.ContextIdle)
		{
			if (_dispatcher == null)
				_dispatcher = Application.Current.Dispatcher;

			_dispatcher.BeginInvoke(priority, action);
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;
	

		protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}

		protected void OnPropertyChanged<T>(Expression<Func<T>> property)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				var propertyName = GetPropertyName(property);
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		private string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
		{
			if (propertyExpression == null)
				throw new ArgumentNullException("propertyExpression");

			var memberExpression = propertyExpression.Body as MemberExpression;
			if (memberExpression == null)
				throw new ArgumentException("Invalid argument", "propertyExpression");

			var propertyInfo = memberExpression.Member as PropertyInfo;
			if (propertyInfo == null)
				throw new ArgumentException("Argument is not a property", "propertyExpression");

			return propertyInfo.Name;
		}
	}
}
