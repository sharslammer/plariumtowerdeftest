﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PlariumTowerDeffense.Wpf.Common.Customization
{
	class TemplateSelector: DataTemplateSelector
	{
		private readonly string groupId;
		private TemplateBinderService _context;		

		public TemplateSelector(string groupId)
		{
			this.groupId = groupId;
			_context = IocMock.TemplateBinder;
		}

		public override DataTemplate SelectTemplate(object item, DependencyObject container)
		{
			if (item == null)
				return null;

			//base WPF search
			var template = base.SelectTemplate(item, container);
			if (template != null)
				return template;

			//IocMock resolver
			var key = string.Concat(item.GetType(), this.groupId);
			template = _context.ResolveDataTemplate(key);
			
			return template;
		}				
	}	
}
