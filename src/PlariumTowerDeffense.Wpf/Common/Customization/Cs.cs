﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace PlariumTowerDeffense.Wpf.Common.Customization
{
	/// <summary>
	/// 
	/// </summary>
	public class Cs : MarkupExtension
	{
		private string groupId = string.Empty;

		public Cs() { }

		/// <summary>
		/// Группа, в которой производится поиск шаблона, см.
		/// </summary>
		public string GroupId
		{
			get { return this.groupId; }
			set { this.groupId = value; }
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return new TemplateSelector(this.GroupId);
		}
	}
}