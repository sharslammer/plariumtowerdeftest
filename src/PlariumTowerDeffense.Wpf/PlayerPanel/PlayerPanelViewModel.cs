﻿using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.Cannons.Configuration;
using PlariumTowerDeffense.MoneyPart;
using PlariumTowerDeffense.Wpf.Common;
using PlariumTowerDeffense.Wpf.InteractionLayer;
using PlariumTowerDeffense.Wpf.PlayGround;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Wpf.PlayerPanel
{
	public class PlayerPanelViewModel : ViewModel
	{
		private readonly ICannonSelectionMediator _selectionMediator;
		private readonly IMoneyControl _moneyControl;
		private readonly IPlayGroundService _playGroundService;
		private readonly ICannonFactory _cannonFactory;
		private readonly ICannonControlMediator _mediator;

		private PlayerCellViewModel _selectedCell;

		public PlayerPanelViewModel(ICannonSelectionMediator selectionMediator, IMoneyControl moneyControl, ICannonControlMediator mediator, ICannonFactory cannonFactory, IEnumerable<CannonConfiguration> configurations)
		{
			_selectionMediator = selectionMediator;
			_selectionMediator.SelectCellAction = SelectCannonCell;
			_moneyControl = moneyControl;
			//_playGroundService = playGroundService;
			_mediator = mediator;
			_cannonFactory = cannonFactory;

			_moneyControl.MoneyChanged += _moneyControl_MoneyChanged;

			AvailableToBuildCannons = LoadCannons(configurations);
			Gold = moneyControl.CurrentGold;
		}

		public int Gold { get; set; }

		public ObservableCollection<AvailableCannonViewModel> AvailableToBuildCannons { get; private set; }

		public bool IsEmptyCell { get; private set; }

		public PlayerCellViewModel SelectedCell
		{
			get { return _selectedCell; }
			set
			{
				_selectedCell = value;
				OnPropertyChanged();

				IsEmptyCell = value.Cannon == null;
				OnPropertyChanged(()=> IsEmptyCell);
			}
		}

		private void _moneyControl_MoneyChanged(object sender, MoneyChangedArgs e)
		{
			//Gold = e.NewValue;
			Gold = _moneyControl.CurrentGold;
			OnPropertyChanged(() => Gold);
		}

		private ObservableCollection<AvailableCannonViewModel> LoadCannons(IEnumerable<CannonConfiguration> configurations)
		{
			return new ObservableCollection<AvailableCannonViewModel>(configurations.Select(x => new AvailableCannonViewModel(x, BuyCannon)));
		}

		private void SelectCannonCell(PlayerCellViewModel cell)
		{
			SelectedCell = cell;
		}

		private void BuyCannon(CannonConfiguration configuration)
		{
			bool canBuy = _moneyControl.BuyCannon(configuration.Cost);
			if(canBuy)
			{
				var cannon = _cannonFactory.CreateCannon(configuration.CannonName);
				if (cannon != null)
				{
					SelectedCell.Cannon = cannon;
					_mediator.RegisterCannon(SelectedCell.Cell, cannon);
				}
			}
		}
	}
}
