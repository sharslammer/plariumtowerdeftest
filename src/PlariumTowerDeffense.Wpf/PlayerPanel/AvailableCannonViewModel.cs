﻿using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.Cannons.Configuration;
using PlariumTowerDeffense.Wpf.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PlariumTowerDeffense.Wpf.PlayerPanel
{
	public class AvailableCannonViewModel
	{
		private readonly Action<CannonConfiguration> _buyAction;
		private CannonConfiguration _configuration;

		public AvailableCannonViewModel(CannonConfiguration configuration, Action<CannonConfiguration> buyAction)
		{
			_buyAction = buyAction;
			_configuration = configuration;

			Name = configuration.CannonName;
			Cost = configuration.Cost;
			CannonCharacteristic = configuration.Characteristic;
		}

		public ICommand CommandBuy { get { return new UiCommand(Buy); } }

		private void Buy()
		{
			if (_buyAction != null)
				_buyAction(_configuration);
		}

		public string Name { get; private set; }

		public int Cost { get; private set; }

		public CannonCharacteristic CannonCharacteristic { get; private set; }

		public object Icon { get; private set; }
	}
}
