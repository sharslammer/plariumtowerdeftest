﻿using PlariumTowerDeffense.Wpf.Common;
using PlariumTowerDeffense.Wpf.PlayGround;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Wpf.InteractionLayer
{

	public class CannonSelectionMediator : ICannonSelectionMediator
	{
		public CannonSelectionMediator()
		{

		}

		public Action<PlayerCellViewModel> SelectCellAction { get; set; }

		public void SelectCell(CellViewModel cell)
		{
			//плоховато, но у нас тут конкретная реализация.
			var selectedCell = cell as PlayerCellViewModel;
			if (selectedCell != null)
				SelectCellAction(selectedCell);
		}

		
	}
}
