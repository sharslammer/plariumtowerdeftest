﻿using PlariumTowerDeffense.RoutePart;
using PlariumTowerDeffense.Wpf.Common;
using PlariumTowerDeffense.Wpf.PlayGround;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Wpf.InteractionLayer
{
	public interface ICannonSelectionMediator
	{
		void SelectCell(CellViewModel cell);

		Action<PlayerCellViewModel> SelectCellAction { get; set; }
	}
}
