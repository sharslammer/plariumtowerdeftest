﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace PlariumTowerDeffense.Wpf
{
	internal class TemplateBinderService
	{
		Dictionary<string, Func<DataTemplate>> _templates = new Dictionary<string, Func<DataTemplate>>();

		internal void BindTemplate<TView, TTemplate>()
			where TView : Control
			where TTemplate : INotifyPropertyChanged
		{
			BindTemplate<TView, TTemplate>(string.Empty);
		}

		internal void BindTemplate<TView, TTemplate>(string templateKey)
			where TView : Control
			where TTemplate : INotifyPropertyChanged
		{
			string key = string.Concat(typeof(TTemplate), templateKey);

			if (!_templates.ContainsKey(key))
				_templates.Add(key, () => CreateDataTemplate(typeof(TView)));
			else
				_templates[key] = () => CreateDataTemplate(typeof(TView));
		}

		internal DataTemplate ResolveDataTemplate(string key)
		{
			if (_templates.ContainsKey(key))
				return _templates[key]();

			return null;
		}

		/// <summary>
		/// Оборачивает представление в <see cref="DataTemplate"/>
		/// </summary>
		/// <param name="viewType">View</param>
		/// <returns><see cref="DataTemplate"/>DataTemplate</returns>
		private DataTemplate CreateDataTemplate(Type viewType)
		{
			var template = new DataTemplate(viewType);
			template.VisualTree = new FrameworkElementFactory(viewType);
			template.Seal();
			return template;
		}
	}
}
