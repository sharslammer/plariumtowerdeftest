﻿using PlariumTowerDeffense.RoutePart;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PlariumTowerDeffense.Wpf.Converters
{
    class MarginFirstCellConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Cell cell = value as Cell;
            Thickness margin = new Thickness(0, 0, 0, 0);
            if (cell.Y == 0 && cell.X % 2 == 1)
                margin.Left += 20;
            return margin;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
