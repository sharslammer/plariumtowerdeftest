﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace PlariumTowerDeffense.Wpf.Converters
{
    class PlayGroundWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value!= null)
            {
                int? w = value as int?;
                if (!w.HasValue)
                    return 0;

                return w.Value * 40+20;
            }
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
