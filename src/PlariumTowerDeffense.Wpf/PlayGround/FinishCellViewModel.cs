﻿using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.RoutePart;
using System;

namespace PlariumTowerDeffense.Wpf.PlayGround
{
	internal class FinishCellViewModel: RoutedCellViewModel
	{
		private RoutedCell rCell;
		private int _lives = 5;
		private Action _onLose;

		public FinishCellViewModel(Cell cell, Action onLose) : base(cell) {
			_onLose = onLose;

			if (_onLose == null)
				throw new ArgumentException("LoseAction not implemented");
		}

		public int Lives
		{
			get { return _lives; }
			set
			{
				_lives = value;
				OnPropertyChanged();
			}
		}



		public override void MonsterEntered(IMonster monster)
		{
			Lives--;

			if (Lives == 0)
				_onLose();
		}
	}
}