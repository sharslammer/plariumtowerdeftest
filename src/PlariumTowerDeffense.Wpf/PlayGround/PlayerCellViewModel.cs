﻿using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.RoutePart;
using PlariumTowerDeffense.Wpf.Common;
using PlariumTowerDeffense.Wpf.InteractionLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PlariumTowerDeffense.Wpf.PlayGround
{
	public class PlayerCellViewModel: CellViewModel
	{
		private readonly ICannonSelectionMediator _mediator;
		private ICannon _cannon;

		public PlayerCellViewModel(Cell cell, ICannonSelectionMediator mediator)
			: base(cell)
		{
			_mediator = mediator;
		}

		public ICommand CommandCellSelect { get { return new UiCommand(CellSelect); } }

		public bool IsEmpty { get { return Cannon == null; } }

		public ICannon Cannon
		{
			get { return _cannon; }
			set
			{
				_cannon = value;
				OnPropertyChanged();
			}
		}

		private void CellSelect()
		{
			_mediator.SelectCell(this);
		}


	}
}
