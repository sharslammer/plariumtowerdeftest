﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlariumTowerDeffense.ApiConfigs;
using System.Collections.ObjectModel;
using PlariumTowerDeffense.RoutePart;
using PlariumTowerDeffense.Wpf.InteractionLayer;

namespace PlariumTowerDeffense.Wpf.PlayGround
{
	public class PlayGroundViewModel
	{
		private ReadOnlyCollection<CellViewModel> _cells;
		private Trail _trail;
		private readonly ICannonSelectionMediator _selectionMediator;
		private IPlayGroundService _playGroundService;
		private Action _loseGame;

		public PlayGroundViewModel()
		{
		}

		public PlayGroundViewModel(IPlayGroundService playGroundService, ICannonSelectionMediator selectionMediator, Action loseGame)
		{
			_playGroundService = playGroundService;
			_selectionMediator = selectionMediator;
			_loseGame = loseGame;

			_trail = _playGroundService.Trail;

			GenerateCells();

			_playGroundService.MonsterMoved += PlayGroundService_MonsterMoved;

		}

		public int GroundWidth { get { return _playGroundService.Width; } }

		public ReadOnlyCollection<CellViewModel> Cells { get { return _cells; } }

		private void GenerateCells()
		{
			List<CellViewModel> list = new List<CellViewModel>(_playGroundService.Height * _playGroundService.Width);
			for (int i = 0; i < _playGroundService.Height * _playGroundService.Width; i++)
			{
				int x = i / _playGroundService.Width;
				int y = i % _playGroundService.Width;

				var rCell = _trail.TrailCells.FirstOrDefault(c => c.X == x && c.Y == y);
				if (rCell != null)
				{
					if (rCell == _trail.FinishCell)
					{
						var model = new FinishCellViewModel(rCell, OnLoseLevel);
						list.Add(model);
					}
					else
					{
						var model = new RoutedCellViewModel(rCell);
						list.Add(model);
					}
				}
				else
				{
					var cell = new Cell(x, y);
					var model = new PlayerCellViewModel(cell, _selectionMediator);
					list.Add(model);
				}
			}
			_cells = new ReadOnlyCollection<CellViewModel>(list);
		}

		private void OnLoseLevel()
		{
			_playGroundService.MonsterMoved -= PlayGroundService_MonsterMoved;
			_loseGame();
		}

		private void PlayGroundService_MonsterMoved(object sender, MoveMonsterEventArgs e)
		{
			var destCellViewModel = _cells.Where(x => x.Cell == e.DestinationCell).FirstOrDefault() as RoutedCellViewModel;
			if (destCellViewModel != null)
			{
				destCellViewModel.MonsterEntered(e.Monster);
			}
			var outCellViewModel = _cells.Where(x => x.Cell == e.OutCell).FirstOrDefault() as RoutedCellViewModel;
			if (outCellViewModel != null)
			{
				outCellViewModel.MonsterCameOut(e.Monster);
			}
		}
	}
}
