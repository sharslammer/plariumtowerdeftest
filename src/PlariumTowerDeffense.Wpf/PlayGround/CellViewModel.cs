﻿using PlariumTowerDeffense.RoutePart;
using PlariumTowerDeffense.Wpf.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlariumTowerDeffense.Monsters;
using System.Collections.ObjectModel;

namespace PlariumTowerDeffense.Wpf.PlayGround
{
    public abstract class CellViewModel : ViewModel
    {
        private Cell _cell;

		public CellViewModel(Cell cell)
        {
            _cell = cell;
        }

        public Cell Cell { get { return _cell; } }

        public int X { get { return _cell.X; } }

        public int Y { get { return _cell.Y; } }
		
        public string Type { get { return _cell.GetType().Name; } }

	}
}
