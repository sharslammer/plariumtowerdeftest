﻿using PlariumTowerDeffense.Monsters;

namespace PlariumTowerDeffense.Wpf.PlayGround
{
	public class MonsterViewModel
	{
		private IMonster _monster;

		public MonsterViewModel(IMonster monster)
		{
			Monster = monster;
		}

		public IMonster Monster { get; private set; }

		public double HP { get; set; }
	}
}