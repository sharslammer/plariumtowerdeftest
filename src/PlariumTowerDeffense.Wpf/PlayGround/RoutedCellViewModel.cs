﻿using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace PlariumTowerDeffense.Wpf.PlayGround
{
	public class RoutedCellViewModel : CellViewModel
	{
		private ObservableCollection<MonsterViewModel> _monsters= new ObservableCollection<MonsterViewModel>();
		private Dispatcher _d;

		public RoutedCellViewModel(Cell cell) : base(cell) {
			_d = Dispatcher.CurrentDispatcher;
		}

		public ObservableCollection<MonsterViewModel> Monsters { get { return _monsters; } }

		public virtual void MonsterEntered(IMonster monster)
		{
			MonsterViewModel model = new MonsterViewModel(monster);
			_d.Invoke(() =>
			_monsters.Add(model));
		}

		public virtual void MonsterCameOut(IMonster monster)
		{
			MonsterViewModel model = _monsters.FirstOrDefault(x => x.Monster == monster);
			if (model != null)
				_d.Invoke(() =>
				_monsters.Remove(model));
		}
	}
}
