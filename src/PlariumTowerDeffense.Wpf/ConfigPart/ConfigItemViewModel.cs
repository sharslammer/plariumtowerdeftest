﻿using PlariumTowerDeffense.Wpf.Common;
using PlariumTowerDeffense.Wpf.InitializationConfigs;
using System.Windows.Input;
using System;

namespace PlariumTowerDeffense.Wpf.ConfigPart
{
	public class ConfigItemViewModel: ViewModel
	{
		private IInititalizationConfigProvider _configProvider;

		public ConfigItemViewModel (IInititalizationConfigProvider configProvider)
		{
			_configProvider = configProvider;
			Header = configProvider.Name;
		}

		public string Header { get; private set; }

		public string Json { get; set; }

		public ICommand CommandSave { get { return new UiCommand(Save); } }

		public void Read()
		{
			using (var reader = _configProvider.ReadFile())
			{
				Json = reader.ReadToEnd();
				OnPropertyChanged(() => Json);
			}
		}

		private void Save()
		{
			try
			{
				_configProvider.UpdateFile(Json);
			}
			catch
			{
				//silent
			}
		}

	}
}