﻿using PlariumTowerDeffense.Wpf.Common;
using PlariumTowerDeffense.Wpf.InitializationConfigs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Wpf.ConfigPart
{
	public class ConfigViewModel: ViewModel
	{
		private ObservableCollection<ConfigItemViewModel> _configItems;
		private IEnumerable<IInititalizationConfigProvider> _configs;
		private int _selectedTab;

		public ConfigViewModel(IEnumerable<IInititalizationConfigProvider> configs)
		{
			_configs = configs;

			_configItems = new ObservableCollection<ConfigItemViewModel>(
				configs.Select(x=>new ConfigItemViewModel(x))
				);
			if(_configItems.Count>0)
				SelectedTab = 0;
		}

		public ObservableCollection<ConfigItemViewModel> ConfigItems { get { return _configItems; } }

		public int SelectedTab
		{
			get { return _selectedTab; }
			set
			{
				_selectedTab = value;
				ConfigItems[_selectedTab].Read();
			}
		}

	}
}
