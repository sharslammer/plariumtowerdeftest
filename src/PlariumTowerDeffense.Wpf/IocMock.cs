﻿using PlariumTowerDeffense.Wpf.InteractionLayer;
using PlariumTowerDeffense.Monsters;
using System.Collections.Generic;
using PlariumTowerDeffense.Monsters.Configuration;
using PlariumTowerDeffense.LevelService;
using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.TimePart;
using PlariumTowerDeffense.ApiConfigs;
using PlariumTowerDeffense.MoneyPart;
using PlariumTowerDeffense.Cannons.Configuration;
using PlariumTowerDeffense.Api.Configuration;

namespace PlariumTowerDeffense.Wpf
{
	/// <summary>
	/// pseudo Ioc to resolve default realizations entities.
	/// </summary>
	public class IocMock
	{
		internal static TemplateBinderService TemplateBinder = new TemplateBinderService();
		private static IMonsterControlMediator _monsterMediator;
		private static ICannonControlMediator _cannonControlMediator;
		private static IPlayGroundService _playGroundService;
		private static IMoneyControl _moneyControl;
		private static ICannonFactory _cannonFactory;

		public static ICannonSelectionMediator ResolveSelectionMediator()
		{
			return new CannonSelectionMediator();
		}

		public static IMonsterFactory ResolveMonsterFactory()
		{
			var configProvider = ResolveConfigProvider<LevelMonsterConfig>();

			var playGroundService = ResolvePlayGroundService();
			return new MonsterFactory(configProvider, playGroundService);
		}

		public static IPlayGroundService ResolvePlayGroundService()
		{
			var configProvider = ResolveConfigProvider<PlayGroundConfig>();

			if (_playGroundService == null)
				_playGroundService = new PlayGroundService.PlayGroundService(configProvider);
			return _playGroundService;
		}

		public static ILevelMainService ResolveLevelMainService()
		{
			var levelMonsterConfigProvider = ResolveConfigProvider<LevelMonsterConfig>();

			var monsterFactory = IocMock.ResolveMonsterFactory();
			var mediator = IocMock.ResolveMonsterContorlMediator();
			return new LevelMainService(levelMonsterConfigProvider, monsterFactory, mediator);
		}

		public static ITimeService ResolveTimeService()
		{
			var mM = IocMock.ResolveMonsterContorlMediator();
			var cM = IocMock.ResolveCannonControlMediator();
			var mC = IocMock.ResolveMoneyControl();
			return new TimeService(cM, mM, mC);
		}

		public static IMoneyControl ResolveMoneyControl()
		{
			if (_moneyControl == null)
			{
				var configProvider = ResolveConfigProvider<LevelConfig>();
				_moneyControl = new MoneyControl(configProvider);
			}
			return _moneyControl;
		}

		public static IMonsterControlMediator ResolveMonsterContorlMediator()
		{
			if (_monsterMediator == null)
			{
				var reward = new MonsterReward(ResolveMoneyControl());
				_monsterMediator = new MonsterControlMediator(reward);
			}
			return _monsterMediator;
		}

		public static ICannonControlMediator ResolveCannonControlMediator()
		{
			var pg = ResolvePlayGroundService();
			if (_cannonControlMediator == null)
				_cannonControlMediator = new CannonControlMediator(pg);
			return _cannonControlMediator;
		}

		public static ICannonFactory ResolveCannonFactory()
		{
			var configProvider = ResolveConfigProvider<LevelCannonConfig>();

			if (_cannonFactory == null)
				_cannonFactory = new CannonFactory(configProvider.Config.CannonConfigurations);
			return _cannonFactory;
		}

		public static IConfigProvider<T> ResolveConfigProvider<T>()
			where T : new()
		{
			return new DefaultConfigProvider<T>();
		}

		/// <summary>
		/// super costilische
		/// </summary>
		internal static void Kill()
		{
			_monsterMediator = null;
			_cannonControlMediator = null;
			_playGroundService = null;
			_moneyControl = null;
			_cannonFactory = null;
		}
	}
}
