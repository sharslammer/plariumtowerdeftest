﻿using PlariumTowerDeffense.Wpf.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace PlariumTowerDeffense.Wpf.MonsterPanel
{
	public class NewWaveInfoViewModel: ViewModel
	{
		private DispatcherTimer _timer;
		private int _secondsToGo;

		public NewWaveInfoViewModel()
		{
			_timer = new DispatcherTimer();
			_timer.Interval = TimeSpan.FromSeconds(1);
			_timer.Tick += _timer_Tick;
		}

		public bool IsWaveWaiting { get; set; }

		public int SecondsToGo
		{
			get { return _secondsToGo; }
			set
			{
				_secondsToGo = value;
				OnPropertyChanged();
			}
		}

		//public Action CountDownEnded { get; set; }

		public void StartWait(int delay)
		{
			SecondsToGo = delay;
			_timer.Start();

			IsWaveWaiting = true;
			OnPropertyChanged(() => IsWaveWaiting);
		}

		private void _timer_Tick(object sender, EventArgs e)
		{
			SecondsToGo--;
			if(SecondsToGo<=0)
			{
				SecondsToGo = 0;
				_timer.Stop();
				OnCountDownEnded();
			}
		}

		private void OnCountDownEnded()
		{
			IsWaveWaiting = false;
			OnPropertyChanged(() => IsWaveWaiting);
		}
	}
}
