﻿using PlariumTowerDeffense.Wpf.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlariumTowerDeffense.LevelService;
using System.Windows.Threading;

namespace PlariumTowerDeffense.Wpf.MonsterPanel
{
	public class MonsterPanelViewModel: ViewModel
	{
		private ILevelMainService _service;

		public MonsterPanelViewModel(ILevelMainService service)
		{
			_service = service;
			_service.NewWaveStarted += _service_NewWaveStarted;
			_service.NewMonsterCreated += _service_NewMonsterCreated;

			NewWaveInfoVm = new NewWaveInfoViewModel();
			//NewWaveInfoVm.CountDownEnded = WaveGenerationEnded;
		}


		public NewWaveInfoViewModel NewWaveInfoVm { get; private set; }

		private void _service_NewMonsterCreated(object sender, MonsterCreatedArgs e)
		{

		}

		private void _service_NewWaveStarted(object sender, NewWaveStartedArgs e)
		{
			NewWaveInfoVm.StartWait(e.Config.Pause);
		}

		//private void WaveGenerationEnded()
		//{
		//	IsWaveWaiting = false;
		//	OnPropertyChanged(() => IsWaveWaiting);
		//}

	}
}
