﻿using PlariumTowerDeffense.Monsters;
using PlariumTowerDeffense.Monsters.Configuration;
using System.Collections.Generic;

namespace PlariumTowerDeffense.Wpf.InitializationConfigs
{
	internal class LevelMonsterConfigInit : InititalizationConfigProvider<LevelMonsterConfig>
	{
		public LevelMonsterConfigInit(string path, LevelMonsterConfig config) : base(path, config)
		{
		}

		protected override LevelMonsterConfig GenerateConfig()
		{
			LevelMonsterConfig levelMonsterConfig = new LevelMonsterConfig()
			{
				Waves = new List<WaveConfig>
				{
					new WaveConfig() {
						Pause = 10 ,
						Portions = new List<MonsterPortion>()
						{
							new MonsterPortion()
							{
								 Delay= 5,
								  MonsterKey= MonsterKeys.CrawlMonster,
								   Quantity = 3,
							},
							//new MonsterPortion()
							//{
							//	 Delay= 2,
							//	  MonsterKey= MonsterKeys.AttackMonster,
							//	   Quantity = 3,
							//},
							new MonsterPortion()
							{
								 Delay= 5,
								  MonsterKey= MonsterKeys.CrawlMonster,
								   Quantity = 4,
							},
						}
					},
					new WaveConfig() {
						Pause = 10 ,
						Portions = new List<MonsterPortion>()
						{
							new MonsterPortion()
							{
								 Delay= 1,
								  MonsterKey= MonsterKeys.CrawlMonster,
								   Quantity = 12,
							},
							new MonsterPortion()
							{
								 Delay= 3,
								  MonsterKey= MonsterKeys.CrawlMonster,
								   Quantity = 8,
							},
							new MonsterPortion()
							{
								 Delay= 1,
								  MonsterKey= MonsterKeys.CrawlMonster,
								   Quantity = 15,
							},
							new MonsterPortion()
							{
								 Delay= 3,
								  MonsterKey= MonsterKeys.CrawlMonster,
								   Quantity = 8,
							},
							new MonsterPortion()
							{
								 Delay= 1,
								  MonsterKey= MonsterKeys.CrawlMonster,
								   Quantity = 15,
							},
						}
					},
				},
				Monsters = new List<MonsterConfiguration>()
				{
					 new MonsterConfiguration()
					 {
						  Name = MonsterKeys.AttackMonster,
						   Cost= 100,
							Health=15,
							 Speed = 6,
					 },
					  new MonsterConfiguration()
					 {
						  Name = MonsterKeys.CrawlMonster,
						   Cost= 50,
							Health=30,
							 Speed = 10,
					 },
				}
			};
			return levelMonsterConfig;
		}
	}
}
