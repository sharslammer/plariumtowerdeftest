﻿using PlariumTowerDeffense.ApiConfigs;
using PlariumTowerDeffense.RoutePart;

namespace PlariumTowerDeffense.Wpf.InitializationConfigs
{
	internal class PlayGroundConfigInit : InititalizationConfigProvider<PlayGroundConfig>
	{
		public PlayGroundConfigInit(string path, PlayGroundConfig config) : base(path, config)
		{
		}

		protected override PlayGroundConfig GenerateConfig()
		{
			PlayGroundConfig pgConfig = new PlayGroundConfig
			{
				FinishCell = new Cell(4, 4),
				Height = 5,
				Width = 5,
				MatrixLine = new int[] {1,1,0,0,0,
										1,1,1,0,0,
										0,1,1,1,1,
										0,0,1,1,1,
										0,0,0,1,1},
			};
			return pgConfig;
		}
	}
}
