﻿using Newtonsoft.Json;
using PlariumTowerDeffense.Api.Configuration;
using System;
using System.IO;

namespace PlariumTowerDeffense.Wpf.InitializationConfigs
{
	internal class InititalizationConfigProvider<T> : IConfigProvider<T>, IInititalizationConfigProvider
	{
		private T _config;
		private string _fileName;
		private string _path;

		public InititalizationConfigProvider(string path, T config)
		{
			_config = config;
			_path = path;
			_fileName = typeof(T).Name + ".json";
		}

		public T Config
		{
			get
			{
				return _config;
			}
		}

		public string Name
		{
			get
			{
				return typeof(T).Name;
			}
		}

		public void CreateConfig()
		{
			string filePath = Path.Combine(_path, _fileName);
			_config = GenerateConfig();

			if (_config!=null)
			{
				using (StreamWriter stream = File.CreateText(filePath))
				using (var jsonTextWriter = new JsonTextWriter(stream))
				{
					JsonSerializer serializer = new JsonSerializer();
					serializer.Formatting = Formatting.Indented;
					serializer.Serialize(jsonTextWriter, Config);
				}
			}
		}

		public StreamReader ReadFile()
		{
			string filePath = Path.Combine(_path, _fileName);
			if (File.Exists(filePath))
			{
				return File.OpenText(filePath);
			}
			else
			{
				throw new ConfigNotExistException(String.Format("Config to {0} at path {1} doesnt exist.", typeof(T).Name, filePath));
			}
		}

		public void Update()
		{
			throw new NotImplementedException();
		}

		public void UpdateFile(string json)
		{
			//try to get Config
			using (TextReader stream = new StringReader(json))
			using (var jsonTextReader = new JsonTextReader(stream))
			{
				JsonSerializer serializer = new JsonSerializer();
				_config = serializer.Deserialize<T>(jsonTextReader);
			}
			if(_config!=null)
			{
				string filePath = Path.Combine(_path, _fileName);

				using (StreamWriter stream = File.CreateText(filePath))
				using (var jsonTextWriter = new JsonTextWriter(stream))
				{
					JsonSerializer serializer = new JsonSerializer();
					serializer.Formatting = Formatting.Indented;
					serializer.Serialize(jsonTextWriter, _config);
				}
			}
		}

		protected virtual T GenerateConfig()
		{
			return _config;
		}

	}
}
