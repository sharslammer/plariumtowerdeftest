﻿using PlariumTowerDeffense.Cannons;
using PlariumTowerDeffense.Cannons.Configuration;
using System.Collections.Generic;

namespace PlariumTowerDeffense.Wpf.InitializationConfigs
{
	internal class LevelCannonConfigInit : InititalizationConfigProvider<LevelCannonConfig>
	{
		public LevelCannonConfigInit(string path, LevelCannonConfig config) : base(path, config)
		{
		}

		protected override LevelCannonConfig GenerateConfig()
		{
			LevelCannonConfig levelCannonConfig = new LevelCannonConfig()
			{
				CannonConfigurations = new List<CannonConfiguration>()
				{
					new CannonConfiguration ()
					{
						CannonName = "SimpleCannon",
						Characteristic = new CannonCharacteristic
						{
							DamageLevel = new CharacteristicItem<int>() { Value = 40, Id = "Damage"},
							HealthPoints = new CharacteristicItem<int>() { Value = 250, Id = "HP"},
							ShootRadius= new CharacteristicItem<int>() { Value = 1, Id = "SR"},
							ShootSpeed= new CharacteristicItem<int>() { Value = 20, Id = "SS"},
							ShootType = new CharacteristicItem<ShootType> { Value = ShootType.OneMonster , Id = "ST" },
						},
						Cost=150,
					},
					new CannonConfiguration ()
					{
						CannonName = "BuckShotCannon",
						Characteristic = new CannonCharacteristic
						{
							DamageLevel = new CharacteristicItem<int>() { Value = 20, Id = "Damage"},
							HealthPoints = new CharacteristicItem<int>() { Value = 250, Id = "HP"},
							ShootRadius= new CharacteristicItem<int>() { Value = 1, Id = "SR"},
							ShootSpeed= new CharacteristicItem<int>() { Value = 20, Id = "SS"},
							ShootType = new CharacteristicItem<ShootType> { Value = ShootType.Cell, Id = "ST" },
						},
						Cost=200,
					}
				}
			};

			return levelCannonConfig;
		}
	}
}
