﻿using System.IO;

namespace PlariumTowerDeffense.Wpf.InitializationConfigs
{
	public interface IInititalizationConfigProvider
	{
		string Name { get; }

		void CreateConfig();

		StreamReader ReadFile();

		void UpdateFile(string json);
	}
}