﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Api.Configuration
{
	public class DefaultConfigProvider<T>: ConfigProvider<T>
	{
		public DefaultConfigProvider(): 
			base(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "config"))
		{

		}
	}
}
