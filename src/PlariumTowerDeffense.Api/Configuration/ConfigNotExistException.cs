﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Api.Configuration
{
	public class ConfigNotExistException : Exception
	{
		public ConfigNotExistException()
		{
		}

		public ConfigNotExistException(string message) : base(message)
		{
		}

		public ConfigNotExistException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected ConfigNotExistException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
