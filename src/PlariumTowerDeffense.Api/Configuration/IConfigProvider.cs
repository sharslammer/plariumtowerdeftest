﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Api.Configuration
{
	public interface IConfigProvider<T>
	{
		T Config { get; }

		/// <summary>
		/// Updates config.
		/// </summary>
		void Update();
	}
}
