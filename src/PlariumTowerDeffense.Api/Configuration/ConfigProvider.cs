﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlariumTowerDeffense.Api.Configuration
{
	public class ConfigProvider<T> : IConfigProvider<T>
	{
		private T _config;
		private string _path;
		private string _fileName;

		public ConfigProvider(string path)
		{
			_path = path;
			_fileName = typeof(T).Name + ".json";
		}

		public T Config
		{
			get
			{
				if (_config == null)
					ReadConfig();
				return _config;
			}
		}

		public void Update()
		{
			if (_config != null)
			{
				SaveConfig();
			}
		}
		
		private void SaveConfig()
		{
			string filePath = Path.Combine(_path, _fileName);

			using (StreamWriter stream = File.CreateText(filePath))
			using (var jsonTextWriter = new JsonTextWriter(stream))
			{
				JsonSerializer serializer = new JsonSerializer();
				serializer.Formatting = Formatting.Indented;
				serializer.Serialize(jsonTextWriter, Config);
			}
		}

		private void ReadConfig()
		{
			string filePath = Path.Combine(_path, _fileName);
			if (File.Exists(filePath))
			{
				using (StreamReader stream = File.OpenText(filePath))
				using (var jsonTextReader = new JsonTextReader(stream))
				{
					JsonSerializer serializer = new JsonSerializer();
					_config = serializer.Deserialize<T>(jsonTextReader);
				}
			}
			else
			{
				throw new ConfigNotExistException(String.Format("Config to {0} at path {1} doesnt exist.", typeof(T).Name, filePath));
			}
		}
	}
}
