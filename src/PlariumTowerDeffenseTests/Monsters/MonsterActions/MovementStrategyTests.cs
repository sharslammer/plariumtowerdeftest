﻿using NUnit.Framework;
using PlariumTowerDeffense.ApiConfigs;
using PlariumTowerDeffense.Monsters.MonsterActions;
using PlariumTowerDeffense.RoutePart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlariumTowerDeffense.PlayGroundService;

namespace PlariumTowerDeffense.Monsters.MonsterActions.Tests
{
	[TestFixture()]
	public class MovementStrategyTests
	{
		MovementStrategy _moveStrategy;
		private PlayGroundService.PlayGroundService _serv;

		[SetUp]
		public void SetupTest()
		{
			PlayGroundConfig config = new PlayGroundConfig
			{
				FinishCell = new Cell(4, 4),
				Height = 5,
				Width = 5,
				MatrixLine = new int[] {1,1,0,0,0,
										1,1,1,0,0,
										0,0,1,1,0,
										0,0,0,1,1,
										0,0,0,1,1},
			};

			//_serv = new PlayGroundService.PlayGroundService(config);
			//_moveStrategy = new MovementStrategy(100, null, _serv);
		}

		[Test()]
		public void NextMoveTest()
		{
			int maxTicks = _serv.Trail.TrailCells.Count;

			RoutedCell finishCell = _serv.Trail.FinishCell;
			int index = 0;
			while (_moveStrategy.CurrentCell != finishCell && index < maxTicks)
			{
				_moveStrategy.NextMove();
				index++;
			}

			Assert.AreEqual(_moveStrategy.CurrentCell,finishCell);
		}
	}
}